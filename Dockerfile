FROM openjdk:8
VOLUME /tmp
ADD target/npaus-0.0.1-SNAPSHOT.jar npaus.jar
EXPOSE 5000
ENTRYPOINT ["java", "-jar", "npaus.jar"]