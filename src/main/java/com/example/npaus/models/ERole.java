package com.example.npaus.models;

public enum ERole {
	ROLE_USER, ROLE_MODERATOR, ROLE_ADMIN
}
