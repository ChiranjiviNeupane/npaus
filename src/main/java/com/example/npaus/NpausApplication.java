package com.example.npaus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NpausApplication {

	public static void main(String[] args) {
		SpringApplication.run(NpausApplication.class, args);
	}

}